import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';

import { AppComponent } from './app.component';
import {CoreModule} from './core/core.module';
import {ProfileModule} from './features/profile/profile.module';
import {AuthModule} from './features/auth/auth.module';
import {HomeModule} from './features/home/home.module';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CoreModule,
    AuthModule,
    ProfileModule,
    HomeModule,
    AppRoutingModule,
    ToastrModule.forRoot() // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
