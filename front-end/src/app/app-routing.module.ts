import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileComponent} from './features/profile/profile.component';
import {LoginComponent} from './features/auth/pages/login/login.component';
import {SignupComponent} from './features/auth/pages/signup/signup.component';
import {ForgotPasswordComponent} from './features/auth/pages/forgot-password/forgot-password.component';
import {ResetPasswordComponent} from './features/auth/pages/reset-password/reset-password.component';
import {HomeComponent} from './features/home/home.component';
import {AuthenticatedGuard} from './core/auth/guards/authenticated.guard';
import {RedirectGuard} from './core/auth/guards/redirect.guard';



const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'home', component:HomeComponent, canActivate: [AuthenticatedGuard]},
  {path: 'login', component: LoginComponent, canActivate: [RedirectGuard]},
  {path: 'signup', component:SignupComponent,  canActivate: [RedirectGuard]},
  {path: 'profile', component:  ProfileComponent, canActivate: [AuthenticatedGuard]},
  {path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [RedirectGuard]},
  {path: 'reset-password', component: ResetPasswordComponent, canActivate: [RedirectGuard]}

];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
