import {Component, OnInit} from '@angular/core';

export interface Diet {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'bh-account',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  //date picker
  minDate = new Date(1919, 0, 1);
  maxDate = new Date(2014, 0, 1);
  //time picker
  minMinute = 0;
  maxMinute = 661;//11hrs
  sliderValue: number = 120;

  foods: Diet[] = [
    {value: 'regular', viewValue: 'Regular'},
    {value: 'vegetarian', viewValue: 'Vegetarian'},
    {value: 'vegan', viewValue: 'Vegan'},
    {value: 'diabetic', viewValue: 'Diabetic'}
  ];

  constructor() {
  }

  ngOnInit() {
  }


  changeTime() {
    let value = this.sliderValue;
    if (!value) {
      return "6:00"
    }
    let hr = 6 + value / 60;
    let min = value % 60;
    return Math.floor(hr) + ":" + ("0" + min).slice(-2);
  }

}
