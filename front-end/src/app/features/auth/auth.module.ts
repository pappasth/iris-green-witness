import { NgModule } from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {LoginComponent} from './pages/login/login.component';
import {ForgotPasswordComponent} from './pages/forgot-password/forgot-password.component';
import {SignupComponent} from './pages/signup/signup.component';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import {ForgotPasswordService} from './services/forgot-password.service';
import {LoginService} from './services/login.service';
import {ResetPasswordService} from './services/reset-password.service';
import {SignupService} from './services/signup.service';
import {ConfirmPasswordService} from './shared/confirm-password.service';

@NgModule({
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    SignupComponent,
    ResetPasswordComponent
  ],
  imports: [
    RouterModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    LoginService,
    SignupService,
    ForgotPasswordService,
    ResetPasswordService,
    ConfirmPasswordService
  ],
  exports: [
    LoginComponent,
    ForgotPasswordComponent,
    SignupComponent,
    ResetPasswordComponent
  ]
})
export class AuthModule { }
