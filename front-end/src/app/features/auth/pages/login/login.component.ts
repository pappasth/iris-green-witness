import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Subscription} from 'rxjs';
import {TokenService} from '../../../../core/auth/services/token.service';
import {UserService} from '../../../../core/auth/services/user.service';
import {LoginService} from '../../services/login.service';

@Component({
  selector: 'bh-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginSubscription: Subscription;
  isAuthenticated: boolean;
  hide = true;
  showSpinner = false;
  formGroup: FormGroup;

  constructor(private router: Router,
              private loginService: LoginService,
              private userService: UserService,
              private tokenUtilitiesService: TokenService,
              private toastr: ToastrService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.userInitialization();
    this.formGroup = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  login(form: FormGroup) {
    this.showSpinner = true;
        // get JWT token from header and initialize the appUser
    const onSuccess = (data) => {
      const token = data.headers.get('Authorization');
      localStorage.setItem("token", token);
      this.userInitialization();
      this.showSpinner = false;
    };
    // display warning
    const onError = (response) => {
      if(response.status == 401){
        this.toastr.warning("Wrong username or password");
      }else if(response.status == 404){
        this.toastr.warning("Service unavailable");
      }
      this.showSpinner = false;
    };
    // request to login
    this.loginSubscription = this.loginService.login(form.controls.username.value, form.controls.password.value)
    .subscribe(onSuccess, onError);
  }


  private userInitialization() {
    const token = localStorage.getItem("token");
    if (token) {
      const payload = this.tokenUtilitiesService.getDecodedToken(token);
      const user = JSON.parse(payload.sub);
      this.userService.setUser(user.username, user.id);
      this.isAuthenticated = true;
      this.router.navigate(["/home"]);

    }
  }

  ngOnDestroy(): void {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }
}
