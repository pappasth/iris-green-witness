import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
/**
 * This is NOT a Global Service
 * It Is a prototype (one instance per injection)
 */
@Injectable()
export class SignupService {

  constructor(private http: HttpClient) {}

  /*
  Request to register a new appUser
  Application server response with appUser's info {id, username, email}
   */
  signup(username, password, email) {
    // POST /api/registration
    return this.http.post(
      "/api/registration",
      {username, password, email},
      {
        headers: new HttpHeaders({'Content-Type': 'application/json'}),
        observe: 'response'
      });
  }

}
