import { TestBed } from '@angular/core/testing';

import { ConfirmPasswordService } from './confirm-password.service';

describe('ConfirmPasswordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmPasswordService = TestBed.get(ConfirmPasswordService);
    expect(service).toBeTruthy();
  });
});
