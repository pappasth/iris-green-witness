import {Injectable, OnDestroy} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Observable} from 'rxjs';
import {UserService} from '../services/user.service';
import {Subscription} from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatedGuard implements CanActivate, OnDestroy {

  loginSubscription: Subscription;

  constructor(private router: Router,
              private userService: UserService,
              private toastr: ToastrService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    this.loginSubscription = this.userService.loggedIn.subscribe(isLoggedIn => {
      if (!isLoggedIn) {
        // this.toastr.warning("Please Log in!");
        this.router.navigate(["/login"]);
      }
    });

    return true;
  }

  ngOnDestroy(): void {
    if (this.loginSubscription) {
      this.loginSubscription.unsubscribe();
    }
  }
}
