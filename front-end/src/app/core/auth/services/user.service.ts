import {EventEmitter, Injectable, Output} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: "root"
})
export class UserService {
  @Output() loggedIn = new EventEmitter<boolean>();

  public username: string;
  public id: number;

  constructor(private router: Router) {
    this.loggedIn.emit(!!localStorage.getItem('token'));
  }


  setUser(username: string, id: number) {
    this.username = username;
    this.id = id;
    this.loggedIn.emit(true);
  }

  logout() {
    localStorage.removeItem("token");
    this.loggedIn.emit(false);
    this.router.navigate(["/login"]);
  }
}
