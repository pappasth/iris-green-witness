import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../auth/services/user.service';

@Component({
  selector: 'bh-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  loggedIn: boolean;

  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(private userService: UserService) {
    this.userService.loggedIn.subscribe(isLoggedIn => {
      this.loggedIn = isLoggedIn;
    });
  }

  ngOnInit() {
    this.loggedIn = false;
  }

  logout() {
    this.userService.logout();
  }

}
