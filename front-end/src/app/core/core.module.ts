import { NgModule } from '@angular/core';
import {AppRoutingModule} from '../app-routing.module';
import {SharedModule} from '../shared/shared.module';
import { NavBarComponent } from './header/nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    NavBarComponent,
  ],
  imports: [
    SharedModule,
    AppRoutingModule
  ],
  exports: [
    NavBarComponent,
  ]
})
export class CoreModule { }
