import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialDesignModule} from './material-design.module';
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  declarations: [
  ],
  imports: [
    MaterialDesignModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ],
  exports: [
    MaterialDesignModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule
  ]
})
export class SharedModule {
}
