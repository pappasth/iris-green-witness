# Iris Green Witness

The objective is to build a simple web app **authorization page** and a **profile page** for logged in
users.

The minimum functionality for the tool is to be able to do the following:  
1. Register a new account  
2. Login and view a restricted page (profile page)  
3. Change password through the profile  
4. Allow the appUser to add custom fields to the personal profile page (key-value pairs)  
5. Reset password via email (Forgot password)  
6. Make the whole tool run inside a Docker container

#### What is implemented
From the above objectives, they are not fully implemented the numbers 3 and 4.

1. In order for a appUser to register he/she has to fill a form with his/her email, username and password.  
After the registration, an email will be sent to his/her email address in order to active the account.  
2. After the account activation the appUser can login to the application an visit only the home page and his/her profile.  
3. In case the appUser does not remember the password, he/she can click on "forgot password" and by giving a mail address, he will receive a mail to reset the password.  
4. The reset password page will only be visible if the url parameter has a valid token.  
5. In the profile page the appUser can see a set of appUser details and appUser preferences BUT these values are NOT stored in Database.  
6. The application consists from 4 docker containers (Application server, Authentication Server, Database, Front-end). Each  of these 4 containers has its own Dockerfile and they are build and linked via a docker-compose file.  


## Prerequisites
**Java** 1.8  [how to install](https://www.java.com/en/download/help/download_options.xml)  
**Apache maven** 3.3.9 [how to install](https://maven.apache.org/install.html)  
**Docker** engine 18.06.0+ [how to install](https://docs.docker.com/install/)  

## Installing & Deployment

#### Build project modules  
Go to root directory of project and run:  
`mvn clean package  `  


#### Build  images and run them in containers
Go to root directory of project and run:  
`docker-compose up`  

open: [http://localhost:4300](http://localhost:4300)

*> Now you are good to go! <* 


## Clean up
In order to remove:
- Containers for services defined in the Compose file
- Networks defined in the networks section of the Compose file
- The default network   

`docker-compose down`  

In order to remove:
 - the above and
 - all images used by any service  

`docker-compose down --rmi all`  

## Exposed endpoints

##### Authentication Server http://localhost:4200

- **/api/auth**  
method: POST  
*Authenticate appUser by username and password. Returns a JWT token in Response Header*

##### Application Server http://localhost:4100

- **/api/registration**  
method: POST  
*Create new appUser by username, password and email. User status initially is set to inactive until it verified by email*

- **/api/registration**  
method: GET  
parameters: token
*Activate an already registered appUser.*

- **/api/password_reset/mail**  
method: POST  
*Send email to the given email address. Email body contains a url with a token parameter, leading to reset-password page*

- **/api/password_reset/valid**  
method: POST  
*If the given token is valid return HTTP status 200, otherwise 404*  


**>The following are committed under the user_profile branch because the are not 100% functional<** 
 
- **/api/password_reset**  
method: POST  
*Reset password of appUser associated with the given token*

- **/api/users/{id}/details**  
method: GET  
*Get appUser's details*


- **/api/users/{id}/details**  
method: POST  
*Update appUser's details*


- **/api/users/{id}/preferences**  
method: GET  
*Get appUser's preferences*


- **/api/users/{id}/preferences**  
method: POST  
*Update appUser's preferences*



