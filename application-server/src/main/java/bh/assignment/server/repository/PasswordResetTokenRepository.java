package bh.assignment.server.repository;

import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.PasswordResetToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
  PasswordResetToken findByToken(String token);
  PasswordResetToken findByAppUser(AppUser appUser);
  }
