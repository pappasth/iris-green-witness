package bh.assignment.server.repository;

import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.VerificationToken;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Serializable> {

  VerificationToken findByToken(String token);
  VerificationToken findByAppUser(AppUser appUser);
}
