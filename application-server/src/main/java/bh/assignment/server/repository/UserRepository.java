

package bh.assignment.server.repository;

import bh.assignment.server.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AppUser entity.
 * {@link AppUser} {@link JpaRepository} extension
 */
@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {
  AppUser findByUsername(String username);
  AppUser findByEmail(String email);
}
