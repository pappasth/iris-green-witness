package bh.assignment.server;

import java.util.Properties;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@SpringBootApplication
public class ApplicationServer {

  @Value("${spring.mail.host}")
  private String mailHost;
  @Value("${spring.mail.port}")
  private int mailPort;
  @Value("${spring.mail.username}")
  private String mailUser;
  @Value("${spring.mail.password}")
  private String mailPass;
  @Value("${spring.mail.properties.mail.smtp.auth}")
  private String smtpAuth;
  @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
  private String smptTtls;
  @Value("${spring.mail.properties.mail.smpt.ssl.trust}")
  private String sslTrust;
  @Value("${spring.mail.properties.mail.transport.protocol}")
  private String mailProtocol;
  @Value("${spring.mail.properties.mail.debug}")
  private String mailDebug;
  public static void main(String[] args) {
    SpringApplication.run(ApplicationServer.class, args);
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }

  @Bean
  public JavaMailSender getJavaMailSender() {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost(mailHost);
    mailSender.setPort(mailPort);

    mailSender.setUsername(mailUser);
    mailSender.setPassword(mailPass);

    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol",mailProtocol);
    props.put("mail.smtp.auth", smtpAuth);
    props.put("mail.smtp.starttls.enable",smptTtls);
    props.put("mail.debug", mailDebug);
    props.put("mail.smtp.ssl.trust", mailHost);

    return mailSender;
  }
}
