package bh.assignment.server.controller;

import bh.assignment.server.dto.UserDTO;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.VerificationToken;
import bh.assignment.server.service.UserService;
import bh.assignment.server.service.VerificationTokenService;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/registration")
public class RegistrationController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final UserService userService;
  private final VerificationTokenService verificationTokenService;
  private final ModelMapper modelMapper;

  @Autowired
  public RegistrationController(UserService userService,
    VerificationTokenService verificationTokenService, ModelMapper modelMapper) {
    this.userService = userService;
    this.verificationTokenService = verificationTokenService;
    this.modelMapper = modelMapper;
    TypeMap<AppUser, UserDTO> typeMap =modelMapper.createTypeMap(AppUser.class, UserDTO.class);
    typeMap.addMappings(mapper -> mapper.skip(UserDTO::setPassword));
  }


  /**
   * Method: POST
   * URI: /api/registration
   * Creates an new {@link AppUser} entity
   * @param  userDto {@link UserDTO} the appUser to be created
   * @return HTTP Response {status: CREATED, body: the created {@link UserDTO} along with its given id}
   */
  @PostMapping
  public ResponseEntity<UserDTO> registerUserAccount(@Valid @RequestBody final UserDTO userDto) {
    logger.debug("New request: POST /api/registration. For appUser {}",userDto.getUsername());
      AppUser registeredAppUser = userService.registerNewUserAccount(convertToEntity(userDto));
    return new ResponseEntity<>(convertToDto(registeredAppUser), HttpStatus.CREATED);
  }

  /**
   * Method: GET
   * URI: /api/registration
   * Parameters: token
   * Activate registered appUser if the provided token exist and it is not expired
   * @param token the token of the confirmation email
   */
  @GetMapping
  public String confirmRegistration(@RequestParam("token") final String token) {
    logger.debug("New request: GET /api/registration. For token {}",token);
    // validate token
    VerificationToken verificationToken = verificationTokenService.isExpired(token);
    AppUser appUser = verificationToken.getAppUser();
    // activate appUser
    userService.activateRegisteredUser(appUser);
    // delete token
    verificationTokenService.delete(verificationToken);
    return "You account is now active. Enjoy!";
  }

  private AppUser convertToEntity(UserDTO dto) {
    return modelMapper.map(dto, AppUser.class);
  }
  private UserDTO convertToDto(AppUser entity) {
    return modelMapper.map(entity, UserDTO.class);
  }

}
