package bh.assignment.server.controller.exception;

import bh.assignment.server.exception.EmailNotSentException;
import bh.assignment.server.exception.ResourceNotFoundException;
import bh.assignment.server.exception.TokenExpiredException;
import bh.assignment.server.exception.UserExistException;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Provide global exception handling for all @restControllers in package "bh.assignment.controller"
 */
@ControllerAdvice("bh.assignment.controller")
@RestController
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @ExceptionHandler(Exception.class)
  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
    log.warn(ex.getMessage());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), ex.getMessage(),
      request.getDescription(false));
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }


  @ExceptionHandler(ResourceNotFoundException.class)
  public final ResponseEntity<Object> handleUserNotFoundException(ResourceNotFoundException ex, WebRequest request) {
    log.warn(ex.getMessage());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), ex.getMessage(),
      request.getDescription(false));
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(UserExistException.class)
  public final ResponseEntity<Object> handleUserExistException(UserExistException ex, WebRequest request) {
    log.warn(ex.getMessage());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), ex.getMessage(),
      request.getDescription(false));
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.CONFLICT, request);
  }

  @ExceptionHandler(TokenExpiredException.class)
  public final ResponseEntity<Object> handleTokenExpiredException(TokenExpiredException ex, WebRequest request) {
    log.warn(ex.getMessage());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), ex.getMessage(),
      request.getDescription(false));
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
  }

  @ExceptionHandler(EmailNotSentException.class)
  public final ResponseEntity<Object> handleEmailNotSentException(EmailNotSentException ex, WebRequest request) {
    log.error(ex.getMessage());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), ex.getMessage(),
      request.getDescription(false));
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.BAD_GATEWAY, request);
  }

  @Override
  protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers,
    HttpStatus status, WebRequest request) {
    log.warn("Validation Failed. {}", ex.getParameterName() + " parameter is missing");
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), "Validation Failed",
      ex.getParameterName() + " parameter is missing");
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
    HttpHeaders headers, HttpStatus status, WebRequest request) {
    log.warn("Validation Failed. {}", ex.getBindingResult());
    ErrorDetails errorDetails = new ErrorDetails(LocalDateTime.now(), "Validation Failed",
      ex.getBindingResult().toString());
    return handleExceptionInternal(ex, errorDetails, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

}
