package bh.assignment.server.controller;


import bh.assignment.server.dto.PasswordForgotDTO;
import bh.assignment.server.dto.PasswordResetDTO;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.PasswordResetToken;
import bh.assignment.server.service.EmailService;
import bh.assignment.server.service.PasswordResetTokenService;
import bh.assignment.server.service.UserService;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/password_reset")
public class PasswordForgotController {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final UserService userService;
  private final EmailService emailService;
  private final PasswordResetTokenService resetTokenService;

  @Autowired
  public PasswordForgotController(UserService userService, EmailService emailService,
    PasswordResetTokenService resetTokenService) {
    this.userService = userService;
    this.emailService = emailService;
    this.resetTokenService = resetTokenService;
  }

  @PostMapping("/mail")
  public ResponseEntity sendMail(@Valid @RequestBody PasswordForgotDTO passwordForgotDTO) {
    logger.debug("New request: POST /api/password_reset/mail. For email {}", passwordForgotDTO.getEmail());
    AppUser appUser = userService.getByEmail(passwordForgotDTO.getEmail());
    if (appUser == null) {
      logger.warn("No appUser found with this email [{}]", passwordForgotDTO.getEmail());
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    emailService.sendPasswordResetEmail(appUser);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PostMapping
  public ResponseEntity resetPassword(@Valid @RequestBody PasswordResetDTO passwordResetDTO) {
    logger.debug("New request: POST /api/password_reset. For token {}", passwordResetDTO.getToken());
    // validate token
    PasswordResetToken resetToken = resetTokenService.isExpired(passwordResetDTO.getToken());
    AppUser appUser = resetToken.getAppUser();
    if (appUser == null) {
      logger.warn("No appUser found correlated to this token [{}]", passwordResetDTO.getToken());
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    // change password
    userService.updatePassword(passwordResetDTO.getPassword(), appUser);
    // delete token
    resetTokenService.delete(resetToken);
    return new ResponseEntity(HttpStatus.OK);
  }

  @PostMapping("/valid")
  public ResponseEntity validateToken(@Valid @RequestBody String token) {
    logger.debug("New request: POST /api/password_reset/valid. For token {}", token);
    // validate token
    PasswordResetToken resetToken = resetTokenService.isExpired(token);
    if (resetToken == null) {
      logger.warn("Now valid token found matching this token [{}]", token);
      return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
    logger.debug("Token [{}] is valid", token);
    return new ResponseEntity(HttpStatus.OK);
  }

}
