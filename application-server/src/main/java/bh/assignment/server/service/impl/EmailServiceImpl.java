package bh.assignment.server.service.impl;

import bh.assignment.server.exception.EmailNotSentException;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.PasswordResetToken;
import bh.assignment.server.model.VerificationToken;
import bh.assignment.server.service.EmailService;
import bh.assignment.server.service.PasswordResetTokenService;
import bh.assignment.server.service.VerificationTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Value("${frontend.url.reset}")
  private String resetURL;
  @Value("${backend.url.activation}")
  private String activationURL;

  private static final String RESET_SUBJECT = "Butler Hotels  - Forgot your password?";
  private static final String ACTIVATION_SUBJECT = "Butler Hotels - Account activation";
  private static final String RESET_BODY = " \"Please click the follow link to reset your password: ";
  private static final String ACTIVATION_BODY = "Please click the follow link to activate your account: ";

  private final JavaMailSender emailSender;
  private final PasswordResetTokenService passwordResetTokenService;
  private final VerificationTokenService verificationTokenService;

  @Autowired
  public EmailServiceImpl(@Qualifier("getJavaMailSender")JavaMailSender emailSender, PasswordResetTokenService passwordResetTokenService,
    VerificationTokenService verificationTokenService) {
    this.emailSender = emailSender;
    this.passwordResetTokenService = passwordResetTokenService;
    this.verificationTokenService = verificationTokenService;
  }

  @Override
  public void sendAccountActivationEmail(AppUser appUser) {
    VerificationToken token = verificationTokenService.createRandomToken(appUser);
    String tokenURL = activationURL + "?token=" + token.getToken();
    logger.debug("Sending account activation email to [{}]", appUser.getEmail());
    sendMail(appUser.getEmail(), ACTIVATION_SUBJECT, ACTIVATION_BODY + tokenURL);
  }

  @Override
  public void sendPasswordResetEmail(AppUser appUser) {
    PasswordResetToken token = passwordResetTokenService.createRandomToken(appUser);
    String tokenURL = resetURL + "?token=" + token.getToken();
    logger.debug("Sending password reset email to [{}]", appUser.getEmail());
    sendMail(appUser.getEmail(), RESET_SUBJECT, RESET_BODY + tokenURL);
  }

  private void sendMail(String to, String subject, String text) {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setTo(to);
    message.setSubject(subject);
    message.setText(text);
    try {
      emailSender.send(message);
    } catch (MailException me) {
      throw new EmailNotSentException(
        String.format("Email could not be send to recipient [%s]", to));
    }
  }
}
