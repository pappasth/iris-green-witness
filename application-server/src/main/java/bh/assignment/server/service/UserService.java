package bh.assignment.server.service;

import bh.assignment.server.exception.UserExistException;
import bh.assignment.server.model.AppUser;

public interface UserService extends AbstractService<AppUser>{

  AppUser updatePassword(String password, AppUser appUser);

  void activateRegisteredUser(AppUser appUser);
  AppUser registerNewUserAccount(AppUser appUser) throws UserExistException;

  AppUser getByEmail(String email);
}
