package bh.assignment.server.service;

import bh.assignment.server.model.PasswordResetToken;
import bh.assignment.server.model.AppUser;

public interface PasswordResetTokenService {

  PasswordResetToken getByToken(String verificationToken);
  void delete(PasswordResetToken verificationToken);
  PasswordResetToken isExpired(String token);
  PasswordResetToken createRandomToken(AppUser appUser);
}
