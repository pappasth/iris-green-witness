package bh.assignment.server.service;

import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.VerificationToken;

public interface VerificationTokenService {

  VerificationToken getByToken(String verificationToken);
  void delete(VerificationToken verificationToken);
  VerificationToken isExpired(String token);
  VerificationToken createRandomToken(AppUser appUser);
}
