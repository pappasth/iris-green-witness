package bh.assignment.server.service.impl;

import bh.assignment.server.exception.ResourceNotFoundException;
import bh.assignment.server.exception.TokenExpiredException;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.PasswordResetToken;
import bh.assignment.server.repository.PasswordResetTokenRepository;
import bh.assignment.server.service.PasswordResetTokenService;
import java.time.LocalDateTime;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final PasswordResetTokenRepository resetTokenRepository;

  @Autowired
  public PasswordResetTokenServiceImpl(PasswordResetTokenRepository resetTokenRepository) {
    this.resetTokenRepository = resetTokenRepository;
  }


  @Override
  public PasswordResetToken getByToken(String verificationToken) {
    PasswordResetToken ret = resetTokenRepository.findByToken(verificationToken);
    if (ret == null) {
      throw new ResourceNotFoundException(
          String.format("Failed to retrieve token. Password reset token [%s] not found.",
              verificationToken));
    } else {
      logger.debug("Password reset token [{}] found. Created on: {}, Expires on: {}",
          verificationToken, ret.getCreatedDate(), ret.getExpiryDate());
      return ret;
    }
  }

  @Override
  public void delete(PasswordResetToken verificationToken) {
    if (resetTokenRepository.existsById(verificationToken.getId())) {
      resetTokenRepository.delete(verificationToken);
      logger.debug("Password reset token [{}] deleted.", verificationToken.getToken());
    } else {
      throw new ResourceNotFoundException(
          String.format("Failed to delete token. Password reset token [%s] not found.",
              verificationToken.getToken()));
    }
  }

  @Override
  public PasswordResetToken isExpired(String token) {
    PasswordResetToken verificationToken = getByToken(token);
    if (verificationToken == null) {
      logger.warn("The provided token [{}] is not found.", token);
      return null;
    } else if (verificationToken.getExpiryDate().compareTo(LocalDateTime.now()) < 0) {
      throw new TokenExpiredException(
          String.format("The provided token [%s] has expired on %s", token,
              verificationToken.getExpiryDate()));
    }
    logger.debug("The provided token [{}] is not expired.", token);
    return verificationToken;
  }


  @Override
  public PasswordResetToken createRandomToken(AppUser appUser) {
    String randomToken = UUID.randomUUID().toString();
    PasswordResetToken newToken = new PasswordResetToken(randomToken, appUser);
    logger.debug("New password reset token created. Token: [{}]", randomToken);
    return resetTokenRepository.saveAndFlush(newToken);
  }
}
