

package bh.assignment.server.service.impl;

import bh.assignment.server.exception.EmailExistsException;
import bh.assignment.server.exception.ResourceNotFoundException;
import bh.assignment.server.exception.UserExistException;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.repository.UserRepository;
import bh.assignment.server.service.EmailService;
import bh.assignment.server.service.UserService;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private final UserRepository userRepository;
  private final EmailService emailService;

  @Autowired
  public UserServiceImpl(UserRepository userRepository, EmailService emailService) {
    this.userRepository = userRepository;
    this.emailService = emailService;
  }

  @Override
  public AppUser updatePassword(String password, AppUser appUser) {
    appUser.setPassword(password);
    logger.debug("AppUser [{}] updated his/her password", appUser.getUsername());
    return userRepository.saveAndFlush(appUser);
  }

  @Override
  public void activateRegisteredUser(AppUser appUser) {
    appUser.setActive(true);
    userRepository.saveAndFlush(appUser);
    logger.info("AppUser [{}] activated.", appUser.getUsername());
  }

  @Override
  public AppUser registerNewUserAccount(AppUser appUser)
    throws UserExistException, EmailExistsException {

    if (userExist(appUser.getUsername())) {
      throw new UserExistException(
        String.format("Username [%s] is reserved", appUser.getUsername()));
    }
    if (emailExist(appUser.getEmail())) {
      throw new EmailExistsException(
        String.format("Account with email [%s], already exist. ", appUser.getEmail()));
    }
    AppUser newAppUser = userRepository.saveAndFlush(appUser);
    logger.info("AppUser [{}] registered", appUser.getUsername());
    emailService.sendAccountActivationEmail(newAppUser);
    return newAppUser;
  }

  @Override
  public AppUser getByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  /**
   * Retrieve the {@link AppUser} associated with the given id.
   *
   * @param id the id whose corresponding entity we want to retrieve.
   * @return the entity retrieved.
   */
  @Override
  public AppUser get(final Long id) {
    return locate(id);
  }

  /**
   * Find all {@link AppUser users}.
   *
   * @return the list of {@link AppUser users}.
   */
  @Override
  public List<AppUser> findAll() {
    return userRepository.findAll();
  }


  /**
   * Gets and return the {@link AppUser} matching the given id, otherwise there's an exception thrown.
   *
   * @param id The id whose matching {@link AppUser} we are seeking for.
   * @return the matching {@link AppUser}.
   */
  private AppUser locate(final Long id) {
    final Optional<AppUser> userOptional = userRepository.findById(id);

    if (!userOptional.isPresent()) {
      throw new ResourceNotFoundException(String.format("AppUser with id %d was not found.", id));
    }
    logger.debug("AppUser with id [{}] found. Username: [{}]", id, userOptional.get().getUsername());
    return userOptional.get();
  }


  private boolean userExist(String username) {
    AppUser appUser = userRepository.findByUsername(username);
    return appUser != null;
  }

  private boolean emailExist(String email) {
    AppUser appUser = userRepository.findByEmail(email);
    return appUser != null;
  }


}
