
package bh.assignment.server.service;

import bh.assignment.server.exception.ResourceNotFoundException;
import java.util.List;

/**
 * A generic interface declaring entity's common actions.
 *
 * @param <T> The domain class whose business we need to implement.
 */
public interface AbstractService<T> {


  /**
   * Get the entity corresponding to the given id.
   * @param id the id whose corresponding entity we want to find.
   * @return the entity.
   */
  T get(Long id) throws ResourceNotFoundException;

  /**
   *Find all containing entities.
   * @return the list of all entities of the given type.
   */
  List<T> findAll();
}
