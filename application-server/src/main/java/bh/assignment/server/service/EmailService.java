package bh.assignment.server.service;

import bh.assignment.server.model.AppUser;

public interface EmailService {

  void sendAccountActivationEmail(AppUser appUser);
  void sendPasswordResetEmail(AppUser appUser);
}
