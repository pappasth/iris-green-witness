package bh.assignment.server.service.impl;

import bh.assignment.server.exception.ResourceNotFoundException;
import bh.assignment.server.exception.TokenExpiredException;
import bh.assignment.server.model.AppUser;
import bh.assignment.server.model.VerificationToken;
import bh.assignment.server.repository.VerificationTokenRepository;
import bh.assignment.server.service.VerificationTokenService;
import java.time.LocalDateTime;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private final VerificationTokenRepository verificationTokenRepository;

  @Autowired
  public VerificationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository) {
    this.verificationTokenRepository = verificationTokenRepository;
  }


  /**
   *  Find a verification token entity by its token
   * @param verificationToken the token
   * @return the entity
   */
  @Override
  public VerificationToken getByToken(String verificationToken) {
    VerificationToken ret = verificationTokenRepository.findByToken(verificationToken);
    if (ret == null) {
      throw new ResourceNotFoundException(
        String.format("Failed to retrieve token. Verification token [%s] not found.", verificationToken));
    } else {
      logger.debug("Verification token [{}] found. Created on: {}, Expires on: {}",
        verificationToken, ret.getCreatedDate(), ret.getExpiryDate());
      return ret;
    }
  }

  /**
   *  Delete a token from the database
   * @param verificationToken the token to be deleted
   */
  @Override
  public void delete(VerificationToken verificationToken) {
    if (verificationTokenRepository.existsById(verificationToken.getId())) {
      verificationTokenRepository.delete(verificationToken);
      logger.debug("Verification token [{}] deleted.", verificationToken.getToken());
    } else {
      throw new ResourceNotFoundException(
        String.format("Failed to delete token. Verification token [%s] not found.", verificationToken.getToken()));
    }
  }

  /**
   *  Check if the given token is valid. In case of it is valid return the entity
   * @param token the token to be validated
   * @return token entity
   */
  @Override
  public VerificationToken isExpired(String token) {
    VerificationToken verificationToken = getByToken(token);
    if (verificationToken.getExpiryDate().compareTo(LocalDateTime.now()) < 0) {
      throw new TokenExpiredException(
        String.format("The provided token [%s] has expired on %s", token, verificationToken.getExpiryDate()));
    }
    logger.debug("The provided token [{}] is not expired.", verificationToken.getToken());
    return verificationToken;
  }

  /**
   *  Create a random token correlated to a appUser
   * @param appUser the appUser which requested the token
   * @return the created token
   */
  @Override
  public VerificationToken createRandomToken(AppUser appUser) {
    String randomToken = UUID.randomUUID().toString();
    VerificationToken newToken = new VerificationToken(randomToken, appUser);
    logger.debug("New verification token created. Token: [{}]", randomToken);
    return verificationTokenRepository.saveAndFlush(newToken);
  }
}

