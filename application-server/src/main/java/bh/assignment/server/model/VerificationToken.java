package bh.assignment.server.model;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;

@Entity
@Data
@ToString
@EqualsAndHashCode(of={"id"})
@AllArgsConstructor
@NoArgsConstructor
public class VerificationToken {

  @Value("${activate.account.minutes}")
  private static final int EXPIRATION = 60*24;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column(nullable = false, unique = true)
  private String token;
  @OneToOne(targetEntity = AppUser.class, fetch = FetchType.EAGER)
  @JoinColumn(name = "userId", nullable = false)
  private AppUser appUser;
  @Column(nullable = false)
  private LocalDateTime createdDate;
  @Column(nullable = false)
  private LocalDateTime expiryDate;

  public VerificationToken(final String token) {
    super();
    this.token = token;
    this.expiryDate = calculateExpiryDate(EXPIRATION);
  }

  public VerificationToken(final String token, final AppUser appUser) {
    super();
    this.token = token;
    this.appUser = appUser;
    this.createdDate = LocalDateTime.now();
    this.expiryDate = calculateExpiryDate(EXPIRATION);
  }

  //TODO maybe this should be moved in a service
  private LocalDateTime calculateExpiryDate(int expiryTimeInMinutes) {
   return   LocalDateTime.now().plusMinutes(expiryTimeInMinutes);
  }
}
