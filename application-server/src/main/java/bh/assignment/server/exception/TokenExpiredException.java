package bh.assignment.server.exception;

public class TokenExpiredException extends RuntimeException {

  public TokenExpiredException(final String message) {
    super(message);
  }
}