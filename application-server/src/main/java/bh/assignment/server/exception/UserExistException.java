package bh.assignment.server.exception;

public class UserExistException extends RuntimeException {

  public UserExistException(final String message) {
    super(message);
  }

}
