

package bh.assignment.server.exception;

/**
 * Custom exception raised when an entity resource is not found.
 */
public class ResourceNotFoundException extends RuntimeException {

  public ResourceNotFoundException(final String message) {
    super(message);
  }
}
