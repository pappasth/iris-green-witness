

package bh.assignment.server.security.jwt;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

public class JwtTokenAuthenticationFilter extends OncePerRequestFilter {
  private final Logger log = LoggerFactory.getLogger(this.getClass());

  private final JwtConfig jwtConfig;

  public JwtTokenAuthenticationFilter(JwtConfig jwtConfig) {
    this.jwtConfig = jwtConfig;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
      FilterChain chain)
      throws ServletException, IOException {

    // get the authentication header. Tokens are passed in the authentication header
    String header = request.getHeader(jwtConfig.getHeader());

    //  validate the header and check the prefix
    if (header == null || !header.startsWith(jwtConfig.getPrefix())) {
      // If not valid, go to the next filter.
      log.error("Invalid header [{}]", header);
      chain.doFilter(request, response);
      return;
    }
    //  Get the token
    String token = header.replace(jwtConfig.getPrefix(), "");
    try {  // exceptions might be thrown in creating the claims if for example the token is expired
      Claims claims = Jwts.parser()
          .setSigningKey(jwtConfig.getSecret().getBytes())
          .parseClaimsJws(token)
          .getBody();

      String username = claims.getSubject();

      if (username != null) {
        log.error("Username [{}] found in JWT token", username);
        @SuppressWarnings("unchecked")
        List<String> authorities = (List<String>) claims.get("authorities");

        //  Create auth object, represents the current authenticated / being authenticated appUser.
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
            username, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(
            Collectors.toList()));

        // Authenticate the appUser
        SecurityContextHolder.getContext().setAuthentication(auth);
      }

    } catch (Exception e) {
      log.error("Could not authenticate appUser. Cause: [{}] ", e.getMessage());
      // Make sure it's clear, so guarantee appUser won't be authenticated
      SecurityContextHolder.clearContext();
    }
    // go to the next filter in the filter chain
    chain.doFilter(request, response);
  }

}
