package bh.assignment.server.security.config;

import bh.assignment.server.security.jwt.JwtConfig;
import bh.assignment.server.security.jwt.JwtTokenAuthenticationFilter;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


  @Autowired
  JwtConfig jwtConfig;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        //  session won't be used to store appUser's state.
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        // handle an authorized attempts
        .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(
        HttpServletResponse.SC_UNAUTHORIZED))
        .and()
        // Add a filter to validate appUser credentials and add token in the response header
        // Add a filter to validate the tokens with every request
        .addFilterAfter(new JwtTokenAuthenticationFilter(jwtConfig),
            UsernamePasswordAuthenticationFilter.class)
        // authorization requests config
        .authorizeRequests()
        // allow all who are accessing "registration" service
        .antMatchers(HttpMethod.POST, "/api/registration/**").permitAll()
      .antMatchers(HttpMethod.GET, "/api/registration/**").permitAll()
      // allow all who are accessing "reset password" service
      .antMatchers(HttpMethod.POST, "/api/password_reset/**").permitAll()
        // any other requests must be authenticated
        .anyRequest().authenticated().and();
  }

}
