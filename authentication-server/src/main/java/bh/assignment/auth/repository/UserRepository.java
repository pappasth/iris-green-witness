

package bh.assignment.auth.repository;

import bh.assignment.auth.model.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AppUser entity.
 */
@Repository
public interface UserRepository extends JpaRepository<AppUser, Long> {
  AppUser findByUsernameAndActiveTrue(String username);
}
