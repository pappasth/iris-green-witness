package bh.assignment.auth.security.jwt;

import bh.assignment.auth.model.AppUser;
import bh.assignment.auth.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.io.IOException;
import java.sql.Date;
import java.util.Collections;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

public class JwtUsernameAndPasswordAuthenticationFilter extends
    UsernamePasswordAuthenticationFilter {

  private final JwtConfig jwtConfig;
  // We use auth manager to validate the appUser credentials
  private AuthenticationManager authManager;
  private UserRepository userRepository;

  public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager,
      JwtConfig jwtConfig, UserRepository userRepository) {
    this.authManager = authManager;
    this.jwtConfig = jwtConfig;
    this.userRepository = userRepository;

    this.setRequiresAuthenticationRequestMatcher(
        new AntPathRequestMatcher(jwtConfig.getUri(), "POST"));
  }

  @Override
  public Authentication attemptAuthentication(
      HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException {

    try {

      // 1. Get credentials from request
      UserCredentials creds = new ObjectMapper()
          .readValue(request.getInputStream(), UserCredentials.class);

      // 2. Create auth object (contains credentials) which will be used by auth manager
      UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
          creds.getUsername(), creds.getPassword(), Collections.emptyList());

      // 3. Authentication manager authenticate the appUser, and use UserDetialsServiceImpl::loadUserByUsername() method to load the appUser.
      return authManager.authenticate(authToken);

    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // Upon successful authentication, generate a token.
  // The 'auth' passed to successfulAuthentication() is the current authenticated appUser.
  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
      FilterChain chain,
      Authentication auth) throws IOException, ServletException {

    JWTSubject subject = buildSubject(auth.getName());
    String jsonSubject = objToJson(subject);

    Long now = System.currentTimeMillis();
    String token = Jwts.builder()
        .setSubject(jsonSubject)
        // Convert to list of strings.
        // This is important because it affects the way we get them back in the Gateway.
        .claim("authorities", auth.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
        .setIssuedAt(new Date(now))
        .setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))  // in milliseconds
        .signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret().getBytes())
        .compact();

    // Add token to header
    response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);
  }

  private JWTSubject buildSubject(String username){
    AppUser appUser = userRepository.findByUsernameAndActiveTrue(username);
    return new JWTSubject(username, appUser.getId().toString());
  }
  private String objToJson(Object object){
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      return "";
    }
  }
  // A (temporary) class just to represent the appUser credentials
  private static class UserCredentials {

    private String username;
    private String password;

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }
  }


  private static class JWTSubject {

    private String username;
    private String id;

    public JWTSubject(String username, String id) {
      this.username = username;
      this.id = id;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }
  }
}
